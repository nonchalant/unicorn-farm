import os
from flask import Flask, current_app, send_file
from flask_mail import Mail
import logging
from logging.handlers import RotatingFileHandler

from .api import api_bp
from .client import client_bp

app = Flask(__name__, static_folder="../dist/static")
app.register_blueprint(api_bp)
# app.register_blueprint(client_bp)

from .config import Config

mail = Mail(app)

# set up logging
handler = RotatingFileHandler("rest_api.log", maxBytes=10000, backupCount=1)
handler.setLevel(logging.DEBUG)
app.logger.addHandler(handler)
app.logger.info(f"Application running in {Config.FLASK_ENV} mode")


@app.route("/")
def index_client():
    dist_dir = current_app.config["DIST_DIR"]
    entry = os.path.join(dist_dir, "index.html")
    return send_file(entry)
