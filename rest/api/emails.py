from flask_mail import Message, Attachment


def send_email(app, msg):
    from rest import mail

    print("Sending email.")

    with app.app_context():
        app.logger.debug(f"Email info: {msg}.")
        mail.send(msg)

    print("Email sent.")


def compose_email(attachments, recipients, subject, template, sender):
    attachment_list = []

    for _, data in attachments.items():
        try:
            attachment = Attachment(data.filename, data.mimetype, data.read())
            attachment_list.append(attachment)
        except Exception as e:
            print(f"Attachment error: {e}\nSaving file to drive instead.")

    return Message(
        subject,
        recipients=recipients,
        html=template,
        sender=sender,
        attachments=attachment_list,
    )


def dispatch_email(recipients, subject, template, attachments={}, sender=None):
    """prepare and send an email to one or more recipients"""
    from rest import app

    if not isinstance(recipients, list):
        recipients = [recipients]

    if not sender:
        app.logger.debug(f"Using default sender: {app.config['MAIL_DEFAULT_SENDER']}")
        sender = app.config["MAIL_DEFAULT_SENDER"]

    msg = compose_email(attachments, recipients, subject, template, sender)
    send_email(app, msg)
