"""
REST API Forms Routing
http://flask-restplus.readthedocs.io
"""
import os

from flask import request, render_template
from flask_restplus import Resource
from threading import Thread
import json

from . import api_rest
from .emails import dispatch_email

# Additional non-config email options
EMAIL_SUBJECT = "A new client just left you a query on The Quote Unicorn"
EMAIL_TEST_ONLY = os.getenv("EMAIL_TEST_ONLY", False)

if EMAIL_TEST_ONLY:
    EMAIL_RECIPIENTS = ["davistodt@gmail.com"]
else:
    EMAIL_RECIPIENTS = ["davistodt@gmail.com", "sean@surelife.co.za"]

print(f"Emails will be sent to: {EMAIL_RECIPIENTS}")


def parse_form_request(form_data):
    form_data = json.loads(form_data)
    user_data = form_data["user information"]
    insurance_data = form_data["insurance information"]

    user_data = {
        "Name": user_data["name"],
        "Surname": user_data["surname"],
        "Email": user_data["email"],
        "ID number": user_data["ID_number"],
        "Contact": user_data["contact"],
        "General comments": user_data["general_comments"],
        "Contact preference": user_data["contact_preference"],
    }

    insurance_data = {
        "Insurance information": {
            k: v for (k, v) in insurance_data.items() if v["isSelected"]
        }
    }

    return insurance_data, user_data


def parse_files_request(files_data):
    return {k: v.filename for (k, v) in files_data.items()}


@api_rest.route("/form")
class ResourceOne(Resource):
    """ Unsecure Resource Class: Inherit from Resource """

    def post(self):
        form_data = request.form.get("data")
        files_data = request.files

        insurance_data, user_data = parse_form_request(form_data)
        insurance_data["Files"] = parse_files_request(files_data)

        template = render_template(
            "mail.html", user_data=user_data, insurance_data=insurance_data
        )

        # spawn async to prevent client timeout after 5 seconds (typically reqs > 1 min)
        Thread(
            target=dispatch_email,
            args=(EMAIL_RECIPIENTS, EMAIL_SUBJECT, template, files_data),
        ).start()

        return {"payload": {}}, 201
