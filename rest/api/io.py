import json


def json_from_file(path: str):
    """Returns the contents of a JSON file"""
    with open(path, "r") as f:
        json_payload = json.load(f)

    return json_payload
