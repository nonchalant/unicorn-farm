"""
Global Flask Application Setting

See `.flaskenv` for default settings.
 """

import os
from rest import app


class Config(object):
    # If not set fall back to production for safety
    FLASK_ENV = os.getenv("FLASK_ENV", "production")
    # Set FLASK_SECRET on your production Environment
    SECRET_KEY = os.getenv("FLASK_SECRET", "Secret")

    APP_DIR = os.path.dirname(__file__)
    ROOT_DIR = os.path.dirname(APP_DIR)
    DIST_DIR = os.path.join(ROOT_DIR, "dist")

    if not os.path.exists(DIST_DIR):
        raise Exception("DIST_DIR not found: {}".format(DIST_DIR))

    # mail settings
    MAIL_SERVER = "smtp.gmail.com"
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_DEBUG = True

    # gmail authentication
    MAIL_USERNAME = os.getenv("MAIL_USERNAME", "thequoteunicorn.sa@gmail.com")
    MAIL_PASSWORD = os.environ["MAIL_PASSWORD"]

    # mail accounts
    MAIL_DEFAULT_SENDER = "thequoteunicorn.sa@gmail.com"


app.config.from_object("rest.config.Config")
