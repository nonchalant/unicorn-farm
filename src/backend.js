import axios from 'axios'

let $axios = axios.create({
  baseURL: '/api/',
  timeout: 5000,
  headers: {'Content-Type': 'application/json'}
})

// Request Interceptor
$axios.interceptors.request.use(function (config) {
  config.headers['Authorization'] = 'Fake Token'
  return config
})

// Response Interceptor to handle and log errors
$axios.interceptors.response.use(function (response) {
  return response
}, function (error) {
  // Handle Error
  console.log(error)
  return Promise.reject(error)
})

// hard-coded as object for the moment
const formData = {
  'Car Insurance': {
    isSelected: false,
    no_file: false,
    file_upload: '',
    additionalComments: ''
  },
  'Vehicle Finance': {
    isSelected: false,
    no_file: false,
    file_upload: '',
    additionalComments: ''
  },
  'Bond': {
    isSelected: false,
    no_file: false,
    file_upload: '',
    additionalComments: ''
  },
  'Household Insurance': {
    isSelected: false,
    no_file: false,
    file_upload: '',
    additionalComments: ''
  },
  'Life Insurance': {
    isSelected: false,
    no_file: false,
    file_upload: '',
    additionalComments: ''
  },
  'Income Protection': {
    isSelected: false,
    no_file: false,
    file_upload: '',
    additionalComments: ''
  }
}

export default {

  fetchResource () {
    return $axios.get(`resource/xxx`)
      .then(response => response.data)
  },

  fetchSecureResource () {
    return $axios.get(`secure-resource/zzz`)
      .then(response => response.data)
  },

  fetchForms () {
    console.debug('fetching forms from static JSON')
    let formDataCopy = JSON.parse(JSON.stringify(formData)) // *copy* to maintain original state
    return formDataCopy
  },

  postNewForm (formData) {
    console.debug('post form to /api/form')
    return $axios.post(`form`, formData, {headers: {'Content-Type': 'multipart/form-data'}})
  }

}
