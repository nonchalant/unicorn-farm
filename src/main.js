import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueScrollTo from 'vue-scrollto'
import VeeValidate from 'vee-validate'
import VueClipboard from 'vue-clipboard2'
import Toasted from 'vue-toasted'
import {library} from '@fortawesome/fontawesome-svg-core'
import {faClipboard} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'

import './filters'

library.add(faClipboard)

Vue.component('font-awesome-icon', FontAwesomeIcon)

// the 'custom' obj supplied to 'dictionary' describes custom field messages
const custom = {
  en: {
    custom: {
      name: {
        required: (field) => 'Your ' + field + ' is required.'
      },
      surname: {
        required: (field) => 'Your ' + field + ' is required.'
      },
      ID_number: {
        required: (field) => 'Your ID number is required.',
        digits: (field) => 'Your ID number should consist of exactly 13 digits.'
      },
      email: {
        required: (field) => 'Your ' + field + ' is required.',
        email: 'Please enter a valid email address.'
      }
    }
  }
}

const validateConfig = {
  dictionary: custom,
  delay: 500
}

Vue.use(VueScrollTo)
Vue.use(VeeValidate, validateConfig)
Vue.use(VueClipboard)
Vue.use(Toasted)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
