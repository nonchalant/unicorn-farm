import Vue from 'vue'
import Vuex from 'vuex'

// imports of AJAX functions go here
import $backend from '@/backend'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // single source of data
    step: 1,
    success: false,
    tileData: {},
    userData: {
      name: '',
      surname: '',
      ID_number: '',
      email: '',
      contact: '',
      contact_preference: 'Email only, please',
      general_comments: ''
    }
  },

  // mutations are operations that actually mutate the state.
  // each mutation handler gets the entire state tree as the
  // first argument, followed by additional payload arguments.
  // mutations must be synchronous and can be recorded by plugins
  // for debugging purposes. Mutations are committed from actions.
  mutations: {
    setStep (state, step) {
      state.step = step
    },
    setSuccess (state, bool) {
      state.success = bool
    },
    toggleTile: function (state, tilename) {
      state.tileData[tilename].isSelected = !state.tileData[tilename].isSelected
    },
    setForms (state, payload) {
      state.tileData = payload
    },
    updateName (state, value) {
      state.userData.name = value
    },
    updateSurname (state, value) {
      state.userData.surname = value
    },
    updateIDNum (state, value) {
      state.userData.ID_number = value
    },
    updateEmail (state, value) {
      state.userData.email = value
    },
    updateContact (state, value) {
      state.userData.contact = value
    },
    updateContactPref (state, value) {
      state.userData.contact_preference = value
    },
    updateFile (state, payload) {
      state.tileData[payload.tile].file_upload = payload.value
    },
    updateNoFile (state, payload) {
      state.tileData[payload.tile].no_file = payload.value
    },
    updateAdditionalComments (state, payload) {
      state.tileData[payload.tile].additionalComments = payload.value
    },
    updateGeneralComments (state, value) {
      state.userData.general_comments = value
    }
  },

  // actions are functions that cause side effects and can involve
  // asynchronous operations. Actions are dispatched from components.
  actions: {
    //
    changeStep (context, step) {
      context.commit('setStep', step)
    },
    setSuccess (context, bool) {
      context.commit('setSuccess', bool)
    },
    toggleTile (context, tilename) {
      context.commit('toggleTile', tilename)
    },
    loadForms (context) {
      context.commit('setForms', $backend.fetchForms())
    },
    submitForm (context) {
      let payload = this.getters.prepareFormData()
      return $backend.postNewForm(payload)
    }
  },

  // reusable data accessors
  getters: {
    isSelectedTile: (state) => (tilename) => {
      return !!state.tileData[tilename].isSelected // true if isSelected is true
    },
    selectedTiles: (state) => () => {
      let selectedTiles = []
      for (let i = 0; i in Object.keys(state.tileData); i++) {
        if (state.tileData[Object.keys(state.tileData)[i]].isSelected) {
          selectedTiles.push(Object.keys(state.tileData)[i])
        }
      }
      return selectedTiles
    },
    prepareFormData: (state) => () => {
      // Iterate over any uploaded files and append the files and append to form data
      let formData = new FormData()
      for (var i = 0; i < Object.keys(state.tileData).length; i++) {
        if (state.tileData[Object.keys(state.tileData)[i]].file_upload) {
          let file = state.tileData[Object.keys(state.tileData)[i]].file_upload
          formData.append(Object.keys(state.tileData)[i], file)
        }
      }

      // Create JSON blob of relevant form data and append to form data
      let payload = {
        'insurance information': state.tileData,
        'user information': state.userData
      }
      formData.append('data', JSON.stringify(payload))

      return formData
    }

  }
})
