import time
import unittest
import urllib3
from pathlib import Path

from flask_testing import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

from rest import app as rest_app

# Set variables for test user
test_user_name = "Rick"
test_user_surname = "Sanchez"
test_user_email = "ricksanchez@earth-C-137.com"
test_user_ID = 100_000_000_0000
test_user_contact = 123_456_7890

# Set variables for test query
test_additional_comments = "Some specific question or comment goes here"
test_file_path = str(Path(".").resolve() / "test_file_for_upload.pdf")

# Set variables for contact information
test_contact_preference = "meet-over-coffee"  # translates to: "Let's meet over coffee"
test_general_comments = "Some general question or comment goes here"


class TestBase(LiveServerTestCase):
    def create_app(self):
        app = rest_app
        app.config.update(
            # Set to testing mode
            TESTING=True,
            # Change the port that the live server listens on
            LIVESERVER_PORT=8943,
        )
        return app

    def setUp(self):
        """Setup the test driver and create test users"""
        options = webdriver.ChromeOptions()
        options.add_argument("headless")
        self.driver = webdriver.Chrome(options=options)
        self.driver.get(self.get_server_url())

        delay = 3  # seconds
        try:
            WebDriverWait(self.driver, delay).until(
                EC.presence_of_element_located((By.ID, "tile1"))
            )
        except TimeoutException:
            print("Page load timed out")

    def tearDown(self):
        self.driver.quit()

    def test_server_is_up_and_running(self):
        http = urllib3.PoolManager()
        url = self.get_server_url()
        response = http.request("GET", url)
        self.assertEqual(response.status, 200)


class TestSuccessfulFormSubmit(TestBase):
    def test_submit_form_no_file_upload(self):
        """
        Test that a user can submit a new query using the query form
        if all fields are filled out correctly, without uploading a file,
        and that they will be redirected to the step 3 view
        """

        # Select one tile to populate step 2 view
        self.driver.find_element_by_id("tile1").click()

        # Click step 2 menu link
        self.driver.find_element_by_id("step2").click()

        # Assert that browser displays view 2 only ("forms-view")
        assert not self.driver.find_element_by_id("tiles-view").is_displayed()
        assert self.driver.find_element_by_id("forms-view").is_displayed()
        assert not self.driver.find_element_by_id("success-view").is_displayed()

        # Fill in registration form
        self.driver.find_element_by_name("name").send_keys(test_user_name)
        self.driver.find_element_by_name("surname").send_keys(test_user_surname)
        self.driver.find_element_by_name("ID_number").send_keys(test_user_ID)
        self.driver.find_element_by_name("email").send_keys(test_user_email)
        self.driver.find_element_by_name("contact").send_keys(test_user_contact)
        self.driver.find_element_by_name("tile1_no_file").send_keys(
            Keys.SPACE
        )  # prevents "Element is not clickable at point..."
        self.driver.find_element_by_name("tile1_additional_comments").send_keys(
            test_additional_comments
        )
        self.driver.find_element_by_name(test_contact_preference).send_keys(Keys.SPACE)
        self.driver.find_element_by_name("general_comments").send_keys(
            test_general_comments
        )
        self.driver.find_element_by_name("submit").click()
        time.sleep(1)

        # Assert that browser displays view 3 only ("forms-view")
        assert not self.driver.find_element_by_id("tiles-view").is_displayed()
        assert not self.driver.find_element_by_id("forms-view").is_displayed()
        assert self.driver.find_element_by_id("success-view").is_displayed()

    def test_submit_form_with_file_upload(self):
        """
        Test that a user can submit a new query using the query form
        if all fields are filled out correctly and a file has been uploaded,
        and that they will be redirected to the step 3 view
        """

        # Select one tile to populate step 2 view
        self.driver.find_element_by_id("tile1").click()

        # Click step 2 menu link
        self.driver.find_element_by_id("step2").click()

        # Assert that browser displays view 2 only ("forms-view")
        assert not self.driver.find_element_by_id("tiles-view").is_displayed()
        assert self.driver.find_element_by_id("forms-view").is_displayed()
        assert not self.driver.find_element_by_id("success-view").is_displayed()

        # Fill in registration form
        self.driver.find_element_by_name("name").send_keys(test_user_name)
        self.driver.find_element_by_name("surname").send_keys(test_user_surname)
        self.driver.find_element_by_name("ID_number").send_keys(test_user_ID)
        self.driver.find_element_by_name("email").send_keys(test_user_email)
        self.driver.find_element_by_name("contact").send_keys(test_user_contact)
        self.driver.find_element_by_name("tile1_file_upload").send_keys(test_file_path)
        time.sleep(2)
        self.driver.find_element_by_name("tile1_additional_comments").send_keys(
            test_additional_comments
        )
        self.driver.find_element_by_name(test_contact_preference).send_keys(Keys.SPACE)
        self.driver.find_element_by_name("general_comments").send_keys(
            test_general_comments
        )
        self.driver.find_element_by_name("submit").click()
        time.sleep(1)

        # Assert that browser displays view 3 only ("forms-view")
        assert not self.driver.find_element_by_id("tiles-view").is_displayed()
        assert not self.driver.find_element_by_id("forms-view").is_displayed()
        assert self.driver.find_element_by_id("success-view").is_displayed()


if __name__ == "__main__":
    unittest.main()
