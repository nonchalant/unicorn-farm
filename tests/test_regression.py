import pytest
from pathlib import Path

from flask import url_for
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep

from rest import app as rest_app


# Set variables for test user
test_user_name = "Rick"
test_user_surname = "Sanchez"
test_user_email = "ricksanchez@earth-C-137.com"
test_user_ID = 100_000_000_0000
test_user_contact = 123_456_7890

# Set variables for test query
test_additional_comments = "Some specific question or comment goes here"
test_file_path = str(Path(__file__).parent.resolve() / "test_file_for_upload.pdf")

# Set variables for contact information
test_contact_preference = "meet-over-coffee"  # translates to: "Let's meet over coffee"
test_general_comments = "Some general question or comment goes here"

# Set example of complete form with keystrokes for each field "name" attribute
field_values = {
    "name": test_user_name,
    "surname": test_user_surname,
    "email": test_user_email,
    "ID_number": test_user_ID,
    "tile1_no_file": Keys.SPACE,
}

# Set expected form field errors for each respective field "name" attribute
field_errors = {
    "name": {"id": "field-error-name", "contains_text": "Your name is required"},
    "surname": {
        "id": "field-error-surname",
        "contains_text": "Your surname is required",
    },
    "email": {"id": "field-error-email", "contains_text": "Your email is required"},
    "ID_number": {
        "id": "field-error-ID_number",
        "contains_text": "Your ID number is required",
    },
    "tile1_no_file": {
        "id": "field-error-file-tile1",
        "contains_text": "Please either upload a file or select to request a new "
                         "quote",
    },
}


# From: https://pytest-flask.readthedocs.io/en/latest/tutorial.html#step-2-configure
# For more on testing with selenium and pytest, see:
#  - https://www.lambdatest.com/blog/test-automation-using-pytest-and-selenium-webdriver
#  - https://www.blazemeter.com/blog/improve-your-selenium-webdriver-tests-with-pytest
@pytest.fixture
def app():
    return rest_app


# Fixture for Firefox and Chrome
@pytest.fixture(params=["chrome"], scope="class")
def driver_init(request):
    options = webdriver.ChromeOptions()
    options.add_argument("headless")

    if request.param == "chrome":
        web_driver = webdriver.Chrome(options=options)
    if request.param == "firefox":
        web_driver = webdriver.Firefox(options=options)
    request.cls.driver = web_driver
    yield
    web_driver.close()


@pytest.mark.usefixtures("driver_init")
@pytest.mark.usefixtures("live_server")
class BasicTest:
    pass


class TestURL(BasicTest):
    def test_open_url(self):
        # live_server.start()
        self.driver.get(url_for("index_client", _external=True))
        assert self.driver.title == "Unicorn Farm"

        sleep(5)


class TestSuccessfulFormSubmit(BasicTest):
    def test_submit_form_no_file_upload(self):
        """
        Test that a user can submit a new query using the query form
        if all fields are filled out correctly, without uploading a file,
        and that they will be redirected to the step 3 view
        """
        self.driver.get(url_for("index_client", _external=True))

        # Select one tile to populate step 2 view
        self.driver.find_element_by_id("tile1").click()

        # Click step 2 menu link
        self.driver.find_element_by_id("step2").click()

        # Assert that browser displays view 2 only ("forms-view")
        assert not self.driver.find_element_by_id("tiles-view").is_displayed()
        assert self.driver.find_element_by_id("forms-view").is_displayed()
        assert not self.driver.find_element_by_id("success-view").is_displayed()

        # Fill in registration form
        self.driver.find_element_by_name("name").send_keys(test_user_name)
        self.driver.find_element_by_name("surname").send_keys(test_user_surname)
        self.driver.find_element_by_name("ID_number").send_keys(test_user_ID)
        self.driver.find_element_by_name("email").send_keys(test_user_email)
        self.driver.find_element_by_name("contact").send_keys(test_user_contact)
        self.driver.find_element_by_name("tile1_no_file").send_keys(
            Keys.SPACE
        )  # prevents "Element is not clickable at point..."
        self.driver.find_element_by_name("tile1_additional_comments").send_keys(
            test_additional_comments
        )
        self.driver.find_element_by_name(test_contact_preference).send_keys(Keys.SPACE)
        self.driver.find_element_by_name("general_comments").send_keys(
            test_general_comments
        )
        self.driver.find_element_by_name("submit").click()
        sleep(1)

        # Assert that browser displays view 3 only ("forms-view")
        assert not self.driver.find_element_by_id("tiles-view").is_displayed()
        assert not self.driver.find_element_by_id("forms-view").is_displayed()
        assert self.driver.find_element_by_id("success-view").is_displayed()

    def test_submit_form_with_file_upload(self):
        """
        Test that a user can submit a new query using the query form
        if all fields are filled out correctly and a file has been uploaded,
        and that they will be redirected to the step 3 view
        """
        self.driver.get(url_for("index_client", _external=True))

        # Select one tile to populate step 2 view
        self.driver.find_element_by_id("tile1").click()

        # Click step 2 menu link
        self.driver.find_element_by_id("step2").click()

        # Assert that browser displays view 2 only ("forms-view")
        assert not self.driver.find_element_by_id("tiles-view").is_displayed()
        assert self.driver.find_element_by_id("forms-view").is_displayed()
        assert not self.driver.find_element_by_id("success-view").is_displayed()

        # Fill in registration form
        self.driver.find_element_by_name("name").send_keys(test_user_name)
        self.driver.find_element_by_name("surname").send_keys(test_user_surname)
        self.driver.find_element_by_name("ID_number").send_keys(test_user_ID)
        self.driver.find_element_by_name("email").send_keys(test_user_email)
        self.driver.find_element_by_name("contact").send_keys(test_user_contact)
        self.driver.find_element_by_name("tile1_file_upload").send_keys(test_file_path)
        sleep(2)
        self.driver.find_element_by_name("tile1_additional_comments").send_keys(
            test_additional_comments
        )
        self.driver.find_element_by_name(test_contact_preference).send_keys(Keys.SPACE)
        self.driver.find_element_by_name("general_comments").send_keys(
            test_general_comments
        )
        self.driver.find_element_by_name("submit").click()
        sleep(1)

        # Assert that browser displays view 3 only ("forms-view")
        assert not self.driver.find_element_by_id("tiles-view").is_displayed()
        assert not self.driver.find_element_by_id("forms-view").is_displayed()
        assert self.driver.find_element_by_id("success-view").is_displayed()


class TestUnsuccessfulFormSubmit(BasicTest):
    @pytest.mark.parametrize("param", field_values)
    def test_submit_form_incomplete(self, param):
        """
        Test that a user cannot submit a new query using the query form
        if any of the required fields are incomplete
        """
        self.driver.get(url_for("index_client", _external=True))

        # Select one tile to populate step 2 view
        self.driver.find_element_by_id("tile1").click()

        # Click step 2 menu link
        self.driver.find_element_by_id("step2").click()

        # Assert that browser displays view 2 only ("forms-view")
        assert not self.driver.find_element_by_id("tiles-view").is_displayed()
        assert self.driver.find_element_by_id("forms-view").is_displayed()
        assert not self.driver.find_element_by_id("success-view").is_displayed()
        sleep(1)

        # Fill in query form (only partially)
        for field, data in field_values.items():
            if field != param:
                self.driver.find_element_by_name(field).send_keys(data)

        # Submit incomplete form
        self.driver.find_element_by_name("submit").click()
        sleep(1)

        # Assert that field error is displayed with the appropriate error text
        error_msg = self.driver.find_element_by_id(field_errors[param]["id"])

        assert error_msg.is_displayed()
        assert field_errors[param]["contains_text"] in error_msg.text
